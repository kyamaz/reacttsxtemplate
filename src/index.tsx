import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
//css
import 'spectre.css/dist/spectre.min.css';
import 'spectre.css/dist/spectre-icons.css';
import './index.css';

//redux 
import { Provider } from 'react-redux';
import configureStore from './store/store.config'
export const store = configureStore()
//navigation
import { BrowserRouter, Route, Switch } from 'react-router-dom'

//lazy load module
import Loadable from 'react-loadable';
//lazy loaded routes
function Loading({ error }) {
  if (error) {
    console.log(error)
    return 'Oh nooess!';
  } else {
    return <div className="loading loading-lg"></div>;
  }
}
//important side note
//loadable loading expects component. not just plain jsx
//component must have default export.not nammed export

const App = Loadable({
  loader: () => import('./components/App'),
  loading: Loading
})
// user related pages
const EditUser = Loadable({
  loader: () => import('./containers/user/user'),
  loading: Loading
})
const AddUser = Loadable({
  loader: () => import('./containers/add-user/add-user'),
  loading: Loading
})



ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <section className="container">
        <Switch>
          {/*          
             order is very important. its a switch case, so the most generic at bottom
              */}

          <Route path="/user/add" component={AddUser} />
          <Route path="/user/:id" component={EditUser} />
          <Route path="/" component={App} />

        </Switch>
      </section>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
