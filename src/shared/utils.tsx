

export const formatArrToState = (arr: Array<Object>, ...path:Array<string>): Object => {
    return arr.reduce((acc, curr) => {
      const  key=safeGet(curr, ...path)
      const newFormat = { [key]: curr };
      return { ...acc, ...newFormat };
    }, {});
  };


//format id based object into an array
export const formatedStateToArr=(obj:Object):Array<Object>=>{
    let arr:Array<Object>=[]
     for (let prop in obj){
        arr =[...arr, ...[obj[prop]] ]
     }
     return arr
}
export const safeGet=(value: Object, ...path: string[]): any=> {
    return path.reduce((prev: Object, prop: string) => {
      if (prev && !!prev[prop]) {
        return prev[prop];
      } else {
        return null;
      }
    }, value);
}  


export const normalizeStr=(str:string):string=>str.replace(/\s+/g, "").trim().toUpperCase()
export const chunkData = (data: Array<any>, size: number): Array<any> => {
  return data.reduce((chunks, el, i) => {
    if (i % size === 0) {
      chunks.push([el])
    } else {
      chunks[chunks.length - 1].push(el)
    }
    return chunks
  }, [])
};

export const makeList = (data: Array<any>, maxLen: number = 0): Array<number> => {
  while (data.length < maxLen) {
    const newData = [...data, ...[{ pageIndex: data.length+1}]];
    return makeList(newData, maxLen);
  }
  return data;
};

export const makeParams = (obj:Object):string => {
  if (!obj) {
    return "?";
  }
  const p = Object.keys(obj).reduce((acc, curr) => {
    if (obj.hasOwnProperty(curr)) {
      if (obj[curr] === null || obj[curr] === undefined) {
        return acc;
      }
      return `${acc}&${curr}=${obj[curr]}`;
    }

    return acc;
  }, "");

  return `?${p.slice(1)}&`;
};

//forms
export const fieldKeys=(fields:any):Array<string>=>{
  let keys=[];
  let newArr:any=[];
  for(let prop in fields){
    newArr=[...keys, ...[prop]]
  }
  return newArr
}


export const isEmpty = val =>  val?val.trim() === "":true 
export  const isValidUrl=(u:string):boolean=> u? !!u.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g):false


