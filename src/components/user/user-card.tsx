import * as React from 'react'
import './user-card.css'
import { User } from '@model/index';
import { Link } from 'react-router-dom';

interface Props {
  datum: User
}
const user = (props: Props): JSX.Element => {
  return (
    <div className="column col-3">

      <div className="card">
        <div className="card-image">
          <img src={props.datum.avatar} className="img-responsive centered" />
        </div>
        <div className="card-header">
          <div className="card-title h5 text-center">{props.datum.first_name} {props.datum.last_name} </div>
        </div>
        <div className="card-body">
          <p>some description </p>
        </div>
        <div className="card-footer ">
          <Link className="btn btn-primary centered"
            to={`/user/${props.datum.id}`} >
            update
          </Link>
        </div>
      </div>
    </div>

  )
}

export { user as UserCard }