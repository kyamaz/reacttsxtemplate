import * as React from "react";
import "./nav-back.css";
interface NavBackProps {
  history?: any
}
const navBack = (props: NavBackProps): JSX.Element => (
  <button
    className="btn btn-primary btn-action btn-lg btn-rounded"
   // onClick={props.history.goBack}
  >
    <span className="icon icon-arrow-left"></span>
  </button>

)

export { navBack as Navback };
