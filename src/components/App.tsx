import * as React from "react";
import "./App.css";
import { AppNav } from "./app-nav/app-nav";
import { AppState } from "@store/store.config";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { User } from "@model/index";
import { UserCard } from "./user/user-card";
import { getUsersState } from "@store/select/user.select";
import { StoreAction } from "@model/index"
import { dispatchMaker } from "@store/dispatch/user.dispatch";
import { number } from "prop-types";
import { LOAD_USER } from "@store/actions";

interface AppComponentProps {
  onLoadUsers: Function;
  users: Array<User>;
}
interface AppComponentState { }
class App extends React.Component<AppComponentProps, AppComponentState> {
  constructor(prop) {
    super(prop);
    this.state = {};
  }
  componentDidMount() {
    this.props.onLoadUsers();

  }

  componentDidUpdate(
    prevProps: AppComponentProps,
    prevState: AppComponentState
  ) { }

  public render(): JSX.Element {
    if (!Array.isArray(this.props.users)) {
      return <div></div>
    }
    console.log('USERS')

    return (
      <div className="container">
        {<AppNav />}
        <div className="columns">
          <div className="column col-10" />
          <div className="column col-2 flex-end" />
        </div>
        <div className="columns">
          <div className="addRow">
            <Link className="btn btn-primary" to={`/user/add`}>
              Add user
            </Link>s
          </div>
        </div>
        <div className="columns">
          {
            this.props.users.map(u => <UserCard key={`user__${u.id}`} datum={u} />)
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (
  { users }
    : AppState): { users: Array<User> } => ({
      users: getUsersState(users)
    });
const mapDispatchToProps = dispatch => ({
  onLoadUsers: (pagination: number) => dispatchMaker(LOAD_USER, 1)
});

const ConnectedProducts = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default ConnectedProducts;
