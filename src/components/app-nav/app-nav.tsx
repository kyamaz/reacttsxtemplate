import * as React from 'react'
import { NavLink } from 'react-router-dom'
import './app-nav.css'
const appNav=(props:any):JSX.Element=>{
return(
    <header className="navbar">
    <section className="navbar-section">
        <NavLink className="navbar-brand" exact={true} to='/'>
          home
        </NavLink>

    </section>
  </header>
)
}

export {appNav  as AppNav}