
export type Partial<T> = { [P in keyof T]?: T[P] };

export interface Errors{[key:string]:string}
export interface BaseApiResponse<T>{

    "data": T
}

export interface StoreAction<T>{
    type:string,
    payload:T

}

export interface ApiResponse<T> {
    "page": number,
    "per_page": number,
    "total": number,
    "total_pages": number,
    "data": Array<T>

}
export interface ApiPayload<T>{
    datum:T
}



export interface AppAction {
    type: string;
    payload?: any;
  }
  
