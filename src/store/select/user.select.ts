import { UsersReducerState } from "./../reducers/user.reducer";
import { User } from "@model/index";
import { createSelector } from "reselect";
import { safeGet } from "@shared/utils";
export const getUsersState = createSelector(
  [(userState: { users: Array<User> }) => userState.users],
  (users: Array<User>) => users
);
export const getUserState = createSelector(
  [(userState: { user: User }) => userState.user],
  (state: User) => state
);
export const getUserFirstName = createSelector(
  [(userState: { user: User }) => userState.user.first_name],
  (state: string) => {
    console.log(state, "GETTTTTT");
    return state;
  }
);
