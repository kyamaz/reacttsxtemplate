//users
export const TRY_USER: string = "TRY_USER";

export const LOAD_USER: string = "LOAD_USER";
export const GET_USER: string = "GET_USER";
export const UPDATE_USER: string = "UPDATE_USER";
export const ADD_USER: string = "ADD_USER";
export const USER_LOADED: string = "USER_LOADED";
export const USER_ERROR: string = "USER_ERROR";

export const REDIRECT: string = "REDIRECT";
