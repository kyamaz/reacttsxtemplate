import { USER_LOADED, USER_ERROR } from "./../actions/index";
import { ADD_USER } from "@store/actions";
import { LOAD_USER, GET_USER, UPDATE_USER } from "../actions/index";
import { User, AppAction } from "@model/index";
export interface UsersReducerState {
  users: Array<User>;
  user: User;
  loading: boolean;
  error: any;
}
const initialUsersState = {
  users: [],
  user: undefined,
  loading: false,
  error: undefined
};

const reducer = (
  state: UsersReducerState = initialUsersState,
  action: AppAction
): UsersReducerState => {
  console.log(action);
  switch (action.type) {
    case LOAD_USER: {
      return {
        ...state,
        loading: true
      };
    }
    case USER_LOADED: {
      const { payload } = action;

      const newState = {
        ...state,
        users: [...payload],
        loading: false
      };
      return newState;
    }
    case GET_USER:
    case UPDATE_USER: {
      const { payload } = action;
      const newState = {
        ...state,
        user: {
          ...payload
        }
      };
      return newState;
    }
    case ADD_USER: {
      const { payload } = action;
      const newState = {
        ...state,
        users: [...state.users, ...[payload]]
      };
      return newState;
    }
    case USER_ERROR: {
      const { payload } = action;
      const newState = {
        ...state,
        error: payload
      };
      return newState;
    }
    default:
      return state;
  }
};

export { reducer as UserReducer };
