import * as React from "react";
import "./add-user.css";
//state management
import { connect } from 'react-redux';
import { User, ApiPayload } from "@model/index";
//interface

//comp
import { Navback } from "@components/navBack/nav-back";
import UserForm from "@components/form/user-form/user-form"
import { addUser } from "@store/actions/user.actions";

interface AddUserProps {
    match?: any
    handleOnUpdateUser?: Function,
    initialValues: User,
    history:any
  }

  
  const initialValues:User={
      first_name:undefined,
      last_name:undefined,
      avatar:undefined
  }
  class userPageComponent extends React.Component<AddUserProps, null> {
    constructor(props) {
      super(props)
    }
  
    componentDidMount() {
    }
    componentDidUpdate(prevProps: AddUserProps) { }
    public render(): JSX.Element {
      return (
        <div className='container'>
        <Navback history={this.props.history}/>
          <h1 className="pageTitle">Add profile</h1>
           <UserForm  initialValues={initialValues}  onUpdateUser={this.props.handleOnUpdateUser}  />
        </div>
      )
    }
  }
  

  const mapDispatchToProps = dispatch => (
    {
      handleOnUpdateUser: (payload: ApiPayload<User>) => dispatch(addUser(payload))
  
    }
  )
  const connectedAddUserPage = connect(null, mapDispatchToProps)(userPageComponent)
  export default connectedAddUserPage 