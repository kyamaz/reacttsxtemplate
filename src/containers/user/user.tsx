import * as React from "react";
import "./user.css";
//state management
import { connect } from 'react-redux';
import { User, ApiPayload } from "@model/index";
import { getUser, updateUser } from "@store/actions/user.actions";
//interface

//comp
import { Navback } from "@components/navBack/nav-back";
import UserForm from "@components/form/user-form/user-form"
import { getUserState, getUserFirstName } from "@store/select/user.select";
import { store } from "src";

interface EditUserProps {
  match?: any
  onGetUser?: Function,
  handleOnUpdateUser?: Function,
  initialValues: User,
  history:any
}

interface EditUserState {
  id: undefined,
}


class userPageComponent extends React.Component<EditUserProps, EditUserState> {
  constructor(props) {
    super(props)
    this.state = {
      id: undefined,
    }
  }

  componentDidMount() {
    const { match: { params: { id } } } = this.props;
    const parse = parseInt(id)
    this.setState(prevState => ({ ...prevState, id }))
    this.props.onGetUser(parse)




  }
  componentDidUpdate(prevProps: EditUserProps, prevState: EditUserState) { }
  public render(): JSX.Element {
    return (
      <div className='container'>
      <Navback history={this.props.history}/>
        <h1 className="pageTitle">Edit profile</h1>
         <UserForm  initialValues={this.props.initialValues}  onUpdateUser={this.props.handleOnUpdateUser}/>
      </div>
    )
  }
}

const mapStateToProps = ({users}): { initialValues: User} => (
  {
    initialValues: getUserState(users),
  }
)
const mapDispatchToProps = dispatch => (
  {
    onGetUser: id => dispatch(getUser(id)),
    handleOnUpdateUser: (payload: ApiPayload<User>) => dispatch(updateUser(payload))

  }
)
const connectedEditUserPage = connect(mapStateToProps, mapDispatchToProps)(userPageComponent)
export default connectedEditUserPage 


